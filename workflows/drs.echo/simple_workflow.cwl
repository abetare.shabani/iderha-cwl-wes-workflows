cwlVersion: v1.0
class: CommandLineTool
baseCommand: echo
stdout: output.txt

inputs:
  - id: input
    type: File
    inputBinding:
      position: 1
outputs:
  output:
    type: stdout
